<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2015 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\CustomFields\Tests\Templating\Twig;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\CustomFieldsBundle\Templating\Twig\CustomFieldsGroupRenderingTwig;
use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;

/**
 * Test the rendering of a custom fields group through
 * the `chill_custom_fields_group_widget`
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class CustomFieldsGroupRenderingTwigTest extends KernelTestCase
{
    /**
     *
     * @var CustomFieldsGroupRenderingTwig
     */
    private $cfRendering;
    
    /**
     *
     * @var CustomFieldProvider
     */
    private $cfProvider;
    
    public function setUp()
    {
        self::bootKernel();
        $this->cfRendering = self::$kernel->getContainer()
              ->get('chill.custom_field.twig.custom_fields_group_rendering')
              ;
        
        $this->cfProvider = self::$kernel->getContainer()
              ->get('chill.custom_field.provider');
        
        // set locale to fr
        $prophet = new \Prophecy\Prophet;
        $request = $prophet->prophesize();
        $request->willExtend('Symfony\Component\HttpFoundation\Request');
        $request->getLocale()->willReturn('fr');
        self::$kernel->getContainer()->get('request_stack')
              ->push($request->reveal());
    }
    
    /**
     * 
     * @return CustomField
     */
    private function getSimpleCustomFieldText($slug, $name)
    {
        return (new CustomField())
            ->setSlug($slug)
              ->setName(array('fr' => $name))
              ->setType('text')
              ->setOrdering(10)
              ->setOptions(array("maxLength" => 255))
              ->setActive(true)
              ;
    }
    
    /**
     * 
     * @return CustomFieldsGroup
     */
    private function getCustomFieldsGroup()
    {
        return (new CustomFieldsGroup())
              ->setEntity('\Dummy')
              ->setName(array("fr" => "A cf group"))
              ->addCustomField($this->getSimpleCustomFieldText("horses", "Do you like horses ?."))
              ->addCustomField($this->getSimpleCustomFieldText("sure", "Are you sure ?"))
              ;
    }
    
    public function testRenderingWidget()
    {
        $cfGroup = $this->getCustomFieldsGroup();
        
        $text = $this->cfRendering->renderWidget(array(
           'horses' => 'I like horses',
           'sure' => 'Yes !'
        ), $cfGroup);
        
        $this->assertContains('Do you like horses', $text);
        $this->assertContains('I like horses', $text);
        $this->assertContains('Are you sure', $text);
        $this->assertContains('Yes', $text);
    }
    
    public function testRenderingWidgetDoNotShowEmpty()
    {
        $cfGroup = $this->getCustomFieldsGroup();
        $cfGroup->addCustomField($this->getSimpleCustomFieldText('empty', 'Do not answer'));
        
        $text = $this->cfRendering->renderWidget(array(
           'horses' => 'I like horses',
           'sure' => 'Yes !'
        ), $cfGroup, 'html', array('show_empty' => false));
        
        $this->assertContains('Do you like horses', $text);
        $this->assertContains('I like horses', $text);
        $this->assertContains('Are you sure', $text);
        $this->assertContains('Yes', $text);
        $this->assertNotContains('Do not answer', $text);
    }
}
