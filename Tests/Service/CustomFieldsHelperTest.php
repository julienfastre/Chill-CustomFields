<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2015 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\CustomFields\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\CustomFieldsBundle\Service\CustomFieldsHelper;
use Chill\CustomFieldsBundle\Entity\CustomField;

/**
 * Tests for custom fields helper
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class CustomFieldsHelperTest extends KernelTestCase
{
    /**
     *
     * @var CustomFieldsHelper
     */
    private $cfHelper;
    
    /**
     *
     * @var CustomField
     */
    private $randomCFText;
    
    public function setUp()
    {
        self::bootKernel();
        
        $container = self::$kernel->getContainer();
        
        $this->cfHelper = $container->get('chill.custom_field.helper');
        
        $this->randomCFText = (new CustomField())
                ->setSlug('my-slug')
                ->setActive(true)
                ->setName(array('fr' => 'my cf'))
                ->setOptions(array('maxLength' => 1000))
                ->setType('text')
                ;
    }
    
    public function testRenderCustomField()
    {
        $data = array(
           $this->randomCFText->getSlug() => 'Sample text'
        );
        
        $text = $this->cfHelper->renderCustomField($data, $this->randomCFText);
        
        $this->assertContains('Sample text', $text);
    }
    
    public function testIsEmptyValue()
    {
        // not empty value
        $data = array(
           $this->randomCFText->getSlug() => 'Sample text'
        );
        
        $this->assertFalse($this->cfHelper->isEmptyValue($data, $this->randomCFText));
        
        //empty value
        $data = array(
           $this->randomCFText->getSlug() => ''
        );
        
        $this->assertTrue($this->cfHelper->isEmptyValue($data, $this->randomCFText));
    }
    
}
