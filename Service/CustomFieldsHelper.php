<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2014-2015, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\CustomFieldsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Chill\CustomFieldsBundle\Entity\CustomFieldsGroup;
use Chill\CustomFieldsBundle\Entity\CustomField;

/**
 * Helpers for manipulating custom fields.
 * 
 * Herlpers for getting a certain custom field, for getting the raw value
 * of a custom field and for rendering the value of a custom field.
 * 
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 *
 */
class CustomFieldsHelper
{
    /** @var EntityManagerInterface $em The entity manager */
    private $em;
    
    /** @var CustomFieldProvider $provider Provider of all the declared custom 
     * fields */
    private $provider;
    
    /**
     * Constructor
     * 
     * @param EntityManagerInterface $em The entity manager
     * @param CustomFieldProvider $provider The customfield provider that 
     * contains all the declared custom fields
     */
    public function __construct(EntityManagerInterface $em,
        CustomFieldProvider $provider)
    {
        $this->em = $em;
        $this->provider = $provider;
    }
    
    public function isEmptyValue(array $fields, CustomField $customField)
    {
        $slug = $customField->getSlug();
        $rawValue = (isset($fields[$slug])) ? $fields[$slug] : null;
        
        $customFieldType =  $this->provider->getCustomFieldByType($customField->getType());
        
        return $customFieldType->isEmptyValue($rawValue, $customField);
    }
    
    /**
     * Render the value of a custom field
     * 
     * @param array $fields the **raw** array, as stored in the db
     * @param CustomField $customField the customField entity
     * @param string $documentType The type of document in which the rendered value is displayed ('html' or 'csv').
     * @throws CustomFieldsHelperException if slug is missing
     * @return The representation of the value the customField.
     */
    public function renderCustomField(array $fields, CustomField $customField, $documentType='html')
    {
        $slug = $customField->getSlug();
        $rawValue = (isset($fields[$slug])) ? $fields[$slug] : null;
        $customFieldType =  $this->provider->getCustomFieldByType($customField->getType());
        
        return $customFieldType->render($rawValue, $customField, $documentType);
    }
}